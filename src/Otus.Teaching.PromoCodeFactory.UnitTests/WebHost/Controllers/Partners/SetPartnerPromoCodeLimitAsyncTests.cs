﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests : PartnersBuilder
    {
        private PartnersController _partnersController;
        private readonly Mock<IRepository<Partner>> _partnerMockRepo;
        private readonly SetPartnerPromoCodeLimitRequest _request;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _partnerMockRepo = new Mock<IRepository<Partner>>();
            _request = this.GetPartnerPromoCodeLimitRequest();
        }

        [Fact]
        public async Task Partner_IsNotFound_ReturnsNotFound()
        {
            // Arrange
            Guid guid = new Guid();
            _partnersController = new PartnersController(_partnerMockRepo.Object);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(guid, _request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async Task Partner_Blocked_ReturnsBadRequest()
        {
            // Arrange
            Partner partner = GetPartner();
            partner.IsActive = false;
            _partnerMockRepo.Setup(p => p.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            _partnersController = new PartnersController(_partnerMockRepo.Object);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, _request);

            // Assert
            _partnerMockRepo.Verify(a => a.UpdateAsync(partner), Times.Never);
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }


        [Fact]
        public async Task Partner_SetLimit_ResetPromoCodeNumberIfPreviousNotCancel()
        {
            // Arrange
            Partner partner = GetPartner();
            _partnerMockRepo.Setup(p => p.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            _partnersController = new PartnersController(_partnerMockRepo.Object);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, _request);

            // Assert
            _partnerMockRepo.Verify(a => a.UpdateAsync(partner), Times.Once);
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async Task Partner_GotLimit_StayPromoCodeNumber()
        {
            // Arrange
            Partner partner = GetPartner();
            int lastLimitValue = partner.PartnerLimits.Last().Limit;
            partner.NumberIssuedPromoCodes = lastLimitValue;

            _partnerMockRepo.Setup(p => p.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            _partnersController = new PartnersController(_partnerMockRepo.Object);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, _request);

            // Assert
            _partnerMockRepo.Verify(a => a.UpdateAsync(partner), Times.Once);
            partner.NumberIssuedPromoCodes.Should().Be(lastLimitValue);
        }

        [Fact]
        public async Task Partner_AddLimit_PreviousLimitIsDisabled()
        {
            // Arrange
            Partner partner = GetPartner();
            PartnerPromoCodeLimit lastLimitBeforeAction = partner.PartnerLimits.Last();

            _partnerMockRepo.Setup(p => p.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            _partnersController = new PartnersController(_partnerMockRepo.Object);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, _request);

            // Assert
            _partnerMockRepo.Verify(a => a.UpdateAsync(partner), Times.Once);
            lastLimitBeforeAction.CancelDate.Should().NotBeNull();
        }

        [Fact]
        public async Task Partner_AddLimitWithZeroValue_ReturnsBadRequest()
        {
            // Arrange
            Partner partner = GetPartner();
            _partnerMockRepo.Setup(p => p.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            _partnersController = new PartnersController(_partnerMockRepo.Object);
            _request.Limit = 0;

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, _request);

            // Assert
            _partnerMockRepo.Verify(a => a.UpdateAsync(partner), Times.Never);
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }


        [Fact]
        public async Task Partner_AddLimit_UpdateRepoValue()
        {
            // Arrange
            Partner partner = this.GetPartner();
            _partnerMockRepo.Setup(p => p.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            _partnersController = new PartnersController(_partnerMockRepo.Object);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, _request);

            // Assert
            _partnerMockRepo.Verify(a => a.UpdateAsync(partner), Times.Once);
            partner.PartnerLimits.Last().Limit.Should().Be(_request.Limit);
            partner.PartnerLimits.Last().PartnerId.Should().Be(partner.Id);
            partner.PartnerLimits.Last().EndDate.Should().Be(_request.EndDate);
        }
    }
}