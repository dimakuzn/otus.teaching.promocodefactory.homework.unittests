using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using AutoFixture;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners;

public class PartnersBuilder
{
    protected readonly Fixture _autoFixture;

    public PartnersBuilder()
    {
        _autoFixture = new Fixture();
        _autoFixture.Behaviors.Add(new OmitOnRecursionBehavior(1));
    }

    protected Partner GetPartner()
    {
        ICollection<PartnerPromoCodeLimit> limits = GetPartnerPromoCodeLimit();
        int lastLimitValue = limits.Last().Limit;

        return _autoFixture.Build<Partner>()
            .With(p => p.IsActive, true)
            .With(p => p.NumberIssuedPromoCodes, lastLimitValue - 1)
            .With(p => p.PartnerLimits, limits)
            .Create();
    }

    protected ICollection<PartnerPromoCodeLimit> GetPartnerPromoCodeLimit()
    {
        Random rnd = new Random();
        ICollection<PartnerPromoCodeLimit> res = _autoFixture
            .Build<PartnerPromoCodeLimit>()
            .With(_ => _.CancelDate, DateTime.Now.AddDays(-5))
            .With(_ => _.Limit, rnd.Next(10, 1000))
            .CreateMany<PartnerPromoCodeLimit>(20).ToList();
        var last = res.Last();
        last.CancelDate = null;

        return res;
    }

    protected SetPartnerPromoCodeLimitRequest GetPartnerPromoCodeLimitRequest()
    {
        return _autoFixture
            .Build<SetPartnerPromoCodeLimitRequest>()
            .With(_ => _.EndDate, DateTime.Now.AddDays(5))
            .Create();
    }
}